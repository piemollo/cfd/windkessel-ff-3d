# 3D Windkessel matrix for FreeFem

## How to use it

To download the project repository at a custom destination 
use the `git clone` command.

```bash
git clone https://gitlab.com/piemollo/cfd/windkessel-ff-3d.git
```
or
```bash
git clone https://gitlab.com/piemollo/cfd/windkessel-ff-3d.git <path_to_destination>
```

Alternatively, it can be integrated as a submodule in 
a larger project using the 
[`git submodule`](https://git-scm.com/book/en/v2/Git-Tools-Submodules) function.
To do so, use the following command.

```bash
cd <my_large_project>
mkdir module
git submodule add https://gitlab.com/piemollo/cfd/windkessel-ff-3d.git ./module/
```

Then to use it in the Freefem script, one can use the following
command at the script top if the repository is set at custom 
destination 

```bash
include "<path_to_rep>/windkessel3d.idp"
```
or, if the repository is set as a submodule of the project
 
```bash
include "./module/windkessel3d.idp"
```

## Included macro

This project contains functions to build implicit Windkessel 
matrices given by 
$$ f(\mathbf{u},\mathbf{v}) = 
\int_{\Gamma} \mathbf{u} \cdot \mathbf{n} ~{\rm d}s
\int_{\Gamma} \mathbf{v} \cdot \mathbf{n} ~{\rm d}s,
$$
defined on the boundary $\Gamma$ and where $\mathbf{n}$ is
the unitary external normal.
In FreeFEM, there is at least two ways to deal with fluid velocity fields:
with fields split or with a monolithic approach.
Unlike in the [2D module](https://gitlab.com/piemollo/cfd/windkessel-ff-2d) 
where both approaches are implemented, the 3D module presented here is restricted 
to the monolithic approach.


### Monolithic approach
In the monolithic approach, _i.e._ if one defines the FE space as
```bash
mesh Th = <...>
fespace Yh(Th,[P2,P2,P2,P1]);
```
an implicit Windkessel form leads to a matrix such that
$$
f(\mathbf{u},\mathbf{v}) = 
 \mathbf{v}^t \mathbb{W}
 \mathbf{u},
$$
The command to obtain such matrix using this module is the following
```bash
matrix W(0,0);
int boundlabel = 1;
WindkesselMatrix(Th,boundlabel,W);
```
The matrix obtained here is returned in `W` and will have a size `Yh.ndof`.


## About

A FreeFEM standalone script `test_windkessel.edp` is provided to validate these 
two functions.

The [2D module](https://gitlab.com/piemollo/cfd/windkessel-ff-2d) have been separate in a second project.